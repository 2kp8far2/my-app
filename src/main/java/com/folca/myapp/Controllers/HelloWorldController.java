package com.folca.myapp.Controllers;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//Esto le dice a "SPRING" que nuestra clase es un "Controlador REST" (REST es el tipo de api que vamos a ver en el curso)
public class HelloWorldController {
    @RequestMapping("/") //A SPRING le estamos diciendo que tiene un controlador, el cual tiene un RECURSO que va a
    //recibir una consulta y que va a devolver "Hello world"
    public String helloWorld()
    {
        return "Hello world";
    }
}
