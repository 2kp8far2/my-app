package com.folca.myapp.Controllers;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GoodByeController {
    @PostMapping("/goodbye") //Como argumento va el nombre del recurso
    //El controlador solo va a resolver las consultas solo que vienen con el metodo "POST"
    //Y si no podemos hacer POST desde el navegador abrimos "POSTMAM" y elegimos el metodo a utiilizar
    public String goodbye(){
        return  "good bye world";
    }
}
